A test task from the Kanalservice company on React-native.
1. Login: react
2. Password: qwerty

# Technologies
1. React-native
2. Redux (thunk, redux-toolkit)
3. Styled components
4. Axious

# Getting Started
Clone this repository:

```
git clone https://github.com/IrinRer/Kanalservis_native.git
```

Install dependencies:

```
npm i
```

Start React Native server:

```
npm start
```

Watch on Android:

```
npm run android
```
